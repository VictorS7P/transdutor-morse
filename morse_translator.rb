class MorseTranslator
  def initialize 
    @translated = ""
    @expression = ""
    @next_char = -1
    @has_error = false
  end

  def finish_with_error
    puts "Expressão inválida:\n\"#{@translated}\""
    puts

    listen
  end

  def finish
    puts "Tradução:\n\"#{@translated}\""
    puts

    listen
  end

  def create_state (whenDot, whenTrace, output = "")
    @next_char += 1

    if @next_char >= @expression.length
      @translated += output
      @has_error = @has_error || output == ""

      return @has_error ? finish_with_error : finish
    end

    case @expression[@next_char]
      when "."
        whenDot.call
      when "-"
        whenTrace.call
      when " "
        if output.length
          @translated += output
        else
          @has_error = true
        end

        q0
      when "/"
        @translated += " "

        q0
      else
        error_char
    end
  end

  def create_final_state (output)
    create_state(method(:error_char), method(:error_char), output)
  end

  def error_char
    @translated += '@'
    @has_error = true

    while (@expression[@next_char] != " " && @next_char < @expression.length) do
      @next_char += 1
    end

    q0
  end

  def q0
    create_state(method(:q1), method(:q25))
  end

  def q1
    create_state(method(:q2), method(:q15), 'e')
  end

  def q2
    create_state(method(:q3), method(:q9), 'i')
  end

  def q3
    create_state(method(:q4), method(:q7), 's')
  end

  def q4
    create_state(method(:q5), method(:q6), 'h')
  end

  def q5
    create_final_state('5')
  end

  def q6
    create_final_state('4')
  end

  def q7
    create_state(method(:error_char), method(:q8), 'v')
  end

  def q8
    create_final_state('3')
  end

  def q9
    create_state(method(:q10), method(:q11), 'u')
  end

  def q10
    create_final_state('f')
  end

  def q11
    create_state(method(:q13), method(:q12))
  end

  def q12
    create_final_state('2')
  end

  def q13
    create_state(method(:q14), method(:error_char))
  end

  def q14
    create_final_state('?')
  end

  def q15
    create_state(method(:q16), method(:q21), 'a')
  end

  def q16
    create_state(method(:q17), method(:q18), 'r')
  end

  def q17
    create_final_state('l')
  end

  def q18
    create_state(method(:q19), method(:error_char))
  end

  def q19
    create_state(method(:error_char), method(:q20))
  end

  def q20
    create_final_state('.')
  end

  def q21
    create_state(method(:q22), method(:q23), 'w')
  end

  def q22
    create_final_state('p')
  end

  def q23
    create_state(method(:error_char), method(:q24), 'j')
  end

  def q24
    create_final_state('1')
  end

  def q25
    create_state(method(:q39), method(:q26), 't')
  end

  def q26
    create_state(method(:q33), method(:q27), 'm')
  end

  def q27
    create_state(method(:q31), method(:q28), 'o')
  end

  def q28
    create_state(method(:q30), method(:q29))
  end

  def q29
    create_final_state('0')
  end

  def q30
    create_final_state('9')
  end

  def q31
    create_state(method(:q31), method(:error_char))
  end

  def q32
    create_final_state('8')
  end

  def q33
    create_state(method(:q35), method(:q34), 'g')
  end

  def q34
    create_final_state('q')
  end

  def q35
    create_state(method(:q36), method(:q37), 'z')
  end

  def q36
    create_final_state('7')
  end

  def q37
    create_state(method(:error_char), method(:q38))
  end

  def q38
    create_final_state(',')
  end

  def q39
    create_state(method(:q43), method(:q40), 'n')
  end

  def q40
    create_state(method(:q41), method(:q42), 'k')
  end

  def q41
    create_final_state('c')
  end

  def q42
    create_final_state('y')
  end

  def q43
    create_state(method(:q45), method(:q44), 'd')
  end

  def q44
    create_final_state('x')
  end

  def q45
    create_state(method(:q46), method(:error_char), 'b')
  end

  def q46
    create_state(method(:error_char), method(:q47), '6')
  end

  def q47
    create_final_state('-')
  end

  def listen
    initialize

    puts "Digite uma expressão em código morse"
    @expression = gets.chomp

    q0
  end
end
